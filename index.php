<?php
/**
 * Created by PhpStorm.
 * User: Cinux’
 * Date: 10/07/17
 * Time: 09:15
 */

include './auth.php';
include './config/pages.php';

// Page à afficher par défaut
if (empty($_GET['page']))
    $_GET['page'] = 'home';
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <title>Bienvenue dans votre espace personnel</title>
</head>
<body>
<div align="center">
    <div style="width: 75%;">
        <header>
        <h1>Open Band Booking</h1>
        </header>
        <nav>
        <ul>
            <?php foreach ($page as $item => $value) {
                $classStyle = ($_GET['page'] === $item) ? ' class="currentPage" ' : '';
                echo '<li><a '.$classStyle.'href="index.php?page='.$item.'">'.$value['name'].'</a></li>';
            }?>
        </ul>
        </nav>
        <section class="mainSection">
            <?php
            foreach ($page as $item => $value) {
                if ($_GET['page'] === $item) {
                    echo '<p><h1>' . $value['name'] . '</h1></p>';
                    $incPage = $value['file'];
                    break;
                }
            }
            if (empty($incPage)) { $incPage = $page404; }

            include $incPage;
            ?>
        </section>
    </div>
</div>