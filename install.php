<?php
/**
 * Created by PhpStorm.
 * User: Cinux’
 * Date: 10/07/17
 * Time: 22:13
 */

include "./models/dbAccess.php";

// Administrateur principal du site
$dbPrimaryUser           = 'admin';
$dbPrimaryPassword       = 'admin';
$dbPrimaryMail           = 'test@test.com';
$dbPrimaryLevel          = 3;

// création de l'acces sans charger de base
$dbAccess = new DbAccess(0);

// Création de la base de données
$dbname = 'OpenBB';
$query = 'CREATE DATABASE IF NOT EXISTS '.$dbname;
if (!$dbAccess->exec($query))
    die('La base n\'a pu être créée.<br>');
echo "-> La base a été créée avec succès.<br />";


//Création de l'accès en chargant la base DBNAME
$dbAccess = new DbAccess();



/*******************************************
 *  CRÉATION DE LA TABLE DES UTILISATEURS  *
 *******************************************/

$query = 'CREATE TABLE IF NOT EXISTS '.$dbAccess->getUsersTable().' (
        id tinyint(4) unsigned NOT NULL auto_increment PRIMARY KEY, 
        username varchar(40), 
        mail varchar(100), 
        password varchar(255),
        creationDate int,
        active bool,
        level int unsigned)
';
if (!$dbAccess->exec($query))
    die('Erreur lors de la création de la table: '.$dbAccess->getUsersTable());
echo "-> Table '".$dbAccess->getUsersTable()."' créée avec succès.<br />";

// Création du compte Admin dans la table Users
if (!$dbAccess->addUser($dbPrimaryUser, $dbPrimaryPassword, $dbPrimaryMail, $dbPrimaryLevel))
    echo ("Erreur lors de la création du compte Admin dans la table Users: ".$dbAccess->getAddUserError());
else
    echo "-> L'utilisateur principal a été créé. <br />---- Login: $dbPrimaryUser.<br />---- Pass: $dbPrimaryPassword<br />---- [!] N'oubliez pas de le changer<br />";


/**************************************
 *  CRÉATION DE LA TABLE DES GROUPES  *
 **************************************/

$query = 'CREATE TABLE IF NOT EXISTS '.$dbAccess->getBandsTable().' (
        id tinyint(4) unsigned NOT NULL auto_increment PRIMARY KEY, 
        bamdName varchar(40), 
        mail varchar(100), 
        phone varchar(16),
        membersIds varchar(255),
        creationDate int,
        active bool,
        level int unsigned)
';
if (!$dbAccess->exec($query))
    die('Erreur lors de la création de la table: '.$dbAccess->getBandsTable());
echo "-> Table '".$dbAccess->getBandsTable()."' créée avec succès.<br />";


/************************************
 *  CRÉATION DE LA TABLE DES LIEUX  *
 ************************************/

$query = 'CREATE TABLE IF NOT EXISTS '.$dbAccess->getPlacesTable().' (
        id tinyint(4) unsigned NOT NULL auto_increment PRIMARY KEY, 
        placeName varchar(40), 
        num_and_street varchar (100),
        zipcode int,
        city varchar (30),
        creationDate int,
        active bool,
        level int unsigned)
';
if (!$dbAccess->exec($query))
    die('Erreur lors de la création de la table: '.$dbAccess->getPlacesTable());
echo "-> Table '".$dbAccess->getPlacesTable()."' créée avec succès.<br />";

/***************************************
 *  CRÉATION DE LA TABLE DES CONTACTS  *
 ***************************************/

$query = 'CREATE TABLE IF NOT EXISTS '.$dbAccess->getContactsTable().' (
        id tinyint(4) unsigned NOT NULL auto_increment PRIMARY KEY, 
        contactName varchar(40),
        mail varchar(100), 
        phone1 varchar(16),
        phone2 varchar(16),
        placesIds varchar(255),
        creationDate int,
        active bool,
        level int unsigned)
';
if (!$dbAccess->exec($query))
    die('Erreur lors de la création de la table: '.$dbAccess->getContactsTable());
echo "-> Table '".$dbAccess->getContactsTable()."' créée avec succès.<br />";


/*****************************************
 *  CRÉATION DE LA TABLE DES ÉVÈNEMENTS  *
 *****************************************/

$query = 'CREATE TABLE IF NOT EXISTS '.$dbAccess->getEventsTable().' (
        id tinyint(4) unsigned NOT NULL auto_increment PRIMARY KEY, 
        eventName varchar(40),
        eventDate int,
        playingBandsIds varchar(50),
        description varchar (200),
        placeId tinyint (4),
        creationDate int,
        active bool,
        level int unsigned)
';
if (!$dbAccess->exec($query))
    die('Erreur lors de la création de la table: '.$dbAccess->getEventsTable());
echo "-> Table '".$dbAccess->getEventsTable()."' créée avec succès.<br />";


/*******************************************
 *  CRÉATION DE LA TABLE DES PROSPECTIONS  *
 *******************************************/

$query = 'CREATE TABLE IF NOT EXISTS '.$dbAccess->getProspectionTable().' (
        id tinyint(4) unsigned NOT NULL auto_increment PRIMARY KEY, 
        placeId tinyint(4),
        contactId tinyint(4),
        bandId tinyint(4),
        userId tinyint(4),
        refersTo tinyint(4),
        isRoot bool,
        creationDate int,
        comment varchar (200),
        active bool,
        level int unsigned)
';
if (!$dbAccess->exec($query))
    die('Erreur lors de la création de la table: '.$dbAccess->getProspectionTable());
echo "-> Table '".$dbAccess->getProspectionTable()."' créée avec succès.<br />";


/***************************************
 *  CRÉATION DE LA TABLE DES MESSAGES  *
 ***************************************/

$query = 'CREATE TABLE IF NOT EXISTS '.$dbAccess->getMessagesTable().' (
        id tinyint(4) unsigned NOT NULL auto_increment PRIMARY KEY, 
        senderId tinyint(4),
        destinationUserId tinyint(4),
        destinationGroupsIds varchar (6),
        creationDate int,
        message varchar (400),
        refersTo tinyint(4),
        isRoot bool,
        readed bool,
        level int unsigned)
';
if (!$dbAccess->exec($query))
    die('Erreur lors de la création de la table: '.$dbAccess->getMessagesTable());
echo "-> Table '".$dbAccess->getMessagesTable()."' créée avec succès.<br />";




echo "<p> N'oubliez pas de supprimer le fichier install.php.";