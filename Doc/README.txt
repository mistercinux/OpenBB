ETAT ACTUEL
"""""""""""

Les tables sont crées à l'install et le compte admin est créé
Ul faudra ajouter l'IP à la session pour plus de sécurité;


Open Band Booking
"""""""""""""""""
Platforme locale et participative de booking

Ce projet a pour but de mettre en commun les différents lieux de concerts connus par les différents groupes
de musique des vosges du nord. Il doit ainsi faciliter les démarches et permettre un suivi du
démarchage en cours et passés.


Gestion des droits
- niveau 1: création d'objets, modification de ses propres objets, supprission de ses propres objets
- niveau 2: + modification et suppression des objets d'autrui
- niveau 3: + création/suppression d'utilisateurs


la base de données devra contenir des tables pour:
- Les utilisateurs
- Les groupes
- Les lieux de concerts (prévoir des flags pour la notation et un flag si il est fermé)
- Les contacts
- Les évènements (actifs, désactivés (corbeille)). ils pourront être supprimés
- Les prospections
- Les requetes à traiter par les admins et les mods (Peut être que le mieux serait d'envoyer un message au groupes admin et mods)
- Les messages internes (qui serviront aussi pour justifier la validation ou le refus d'une requête)


options de profile:
- notation des lieux
- pouvoir masquer des lieux
- Publication des évènements à venir sur une page dédiée.


[ Table users ]


[ Table band ]
