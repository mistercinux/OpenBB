<?php
/**
 * Created by PhpStorm.
 * User: Cinux’
 * Date: 11/07/17
 * Time: 10:26
 */

define('DBACCESS_PHP', true);

class DbAccess {
    // db connexion informations
    private $host               = 'localhost';
    private $dbName             = 'OpenBB';
    private $username           = 'test';
    private $password           = 'pass';
    private $usersTable         = 'users';
    private $bandsTable         = 'bands';
    private $placesTable        = 'places';
    private $contactsTable      = 'contacts';
    private $eventsTable        = 'events';
    private $requestsTable      = 'requests';
    private $messagesTable      = 'messages';
    private $prospectionsTable  = 'prospections';

    private $mysqli;

    // Informations sur les erreurs
    private $addUserError;

    // Méthode de vérification du mot de pass. Si il est correct on crée les vars de session
    public function auth($username, $password)
    {
        $query = "SELECT password FROM $this->usersTable WHERE username = '$username'";
        $req   = $this->mysqli->query($query);

        if ($req !== false) {
            $row = $req->fetch_assoc();
            if (password_verify($password, $row['password'])) {
                $_SESSION['username'] = $username;
                $_SESSION['time'] = time();
                $_SESSION['level'] = $row['level'];
                return 1;
            }
        }
        return 0;
    }

    // Méthode d'ajout d'un utilisateur
    public function addUser($username, $password, $mail, $level, $active=true)
    {
        // Vérification de l'existance de $username dans la table des users
        $query1 = "SELECT id FROM ".$this->usersTable." WHERE username='$username'";
        $res1 = $this->mysqli->query($query1);
        $row = $res1->fetch_assoc();
        if (!empty($row['id'])) {
                $this->addUserError = "L'utilisateur $username existe déjà.<br />";
                return 0;
        }


        // hashage du mot de pass
        $password = password_hash($password, PASSWORD_BCRYPT);

        // Création du compte $username dans la table Users
        $query = "INSERT INTO ".$this->getUsersTable()." (username, mail, password, active, level) 
                  VALUES ('$username', '$mail', '$password', $active, '$level')";
        if (!$this->mysqli->query($query)) {
            $this->addUserError = "Une erreur s'est produite lors de l'insersion de l'utilisateus dans la base.<br />";
            return 0;
        }
        return 1;
    }

    // Méthode d'execution brute de requêtes sql.
    public function exec($query)
    {
        if ($this->mysqli->query($query))
            return 1;

        echo 'Erreur: '.$this->mysqli->errno.': '.$this->mysqli->error.'<br />';
        echo '$query = '.$query.'<br />';
        return 0;
    }

    // getters section;
    public function getUsersTable()       { return $this->usersTable;        }
    public function getBandsTable()       { return $this->bandsTable;        }
    public function getPlacesTable()      { return $this->placesTable;       }
    public function getContactsTable()    { return $this->contactsTable;     }
    public function getEventsTable()      { return $this->eventsTable;       }
    public function getRequestsTable()    { return $this->requestsTable;     }
    public function getMessagesTable()    { return $this->messagesTable;     }
    public function getProspectionTable() { return $this->prospectionsTable; }

    public function getAddUserError()     { return $this->addUserError;      }


    // Lors de la création, si $db = false, on ne charge pas la base DBNAME
    public function __construct($db = true)
    {
        if ($db === true)
            $this->mysqli = new mysqli($this->host, $this->username, $this->password, $this->dbName);
        else
            $this->mysqli = new mysqli($this->host, $this->username, $this->password);

        if ($this->mysqli->connect_errno)
            echo 'erreur lors de la connexion.'.$this->mysqli->connect_errno.' : '.$this->mysqli->connect_error;
    }

    // Pour débug
    /*
    public function __destruct()
    {
        echo '<br />dbAccess->__destruct called';
        $this->mysqli->close();
    }
    */
}