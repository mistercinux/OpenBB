<?php
/**
 * Created by PhpStorm.
 * User: Cinux’
 * Date: 10/07/17
 * Time: 14:27
 */
session_start();

define('LOGIN_PHP', true);
include './auth.php';


?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <title>Page de connexion</title>
</head>
<body>

<section>
    <?php echo $message.'<br />'; ?>
    <form id="login_form" action="login.php" method="post">
        <input type="text" id="username" placeholder="Username" name="username" /><br />
        <input type="password" id="password" placeholder="Password" name="password" /><br />
        <input type="submit" name="send" value="Connexion">
    </form>
</section>


</body>
</html>