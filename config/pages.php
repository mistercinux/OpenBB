<?php
/**
 * Created by PhpStorm.
 * User: Cinux’
 * Date: 13/07/17
 * Time: 09:46
 */


$page = array(
    'home' => array('file' => './pages/home.php',
        'name' => 'Accueil'),
    'messages' => array('file' => './pages/messages.php',
        'name' => 'Messagerie'),
    'booking' => array('file' => './pages/booking.php',
        'name' => 'Suivre mon booking'),
    'places' => array('file' => './pages/places.php',
        'name' => 'Lieux de concerts'),
    'events' => array('file' => './pages/events.php',
        'name' => 'Mes évènements'),
    'bands' => array('file' => './pages/bands.php',
                    'name' => 'Gérer mes groupes'),
);

$page404 = './pages/404.php';