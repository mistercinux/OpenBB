<?php
/**
 * Created by PhpStorm.
 * User: Cinux’
 * Date: 11/07/17
 * Time: 10:17
 */

define('AUTH_PHP', false);
session_start();
$message = '';

// Forcer l'authentification si l'user n'est pas authentifié
if (empty($_SESSION['username'])) {
    // Si le formulaire est validé
    if (($_POST['send'] === 'Connexion')) {
        // Vérification des champs et contrôle du mot de pass
        include './models/dbAccess.php';
        $dbAccess = new DbAccess();
        if (!empty($_POST['username']) && !empty($_POST['password']))
            if ($dbAccess->auth($_POST['username'], $_POST['password'])) {
                header('location: ./index.php');
                die();
            }
            else
                $message = 'Identifiant ou mot de pass incorrect';
        else
            $message = "Tous les champs sonts requis.";
    } elseif ($_SERVER['REQUEST_URI'] !== '/login.php') {
        header('location: ./login.php');
        die();
    }
}
// Sinon quand l'user est authentifié
else {
    if ($_SERVER['REQUEST_URI'] === '/login.php') {
        header('location: ./index.php');
        die();
    }
}