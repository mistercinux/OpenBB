<?php
/**
 * Created by PhpStorm.
 * User: Cinux’
 * Date: 12/07/17
 * Time: 10:36
 */

$accessLevel = 3; // niveau 3 pour les admins
/*
 * Attention !!!
 * Il faudra gérer l'id de niveau 3
 */

include '../auth.php';
include '../models/dbAccess.php';

$dbAccess = new DbAccess();

if (!$dbAccess->addUser('test', 'pass', 'test@toto.fr', 1))
    echo "Une erreur est survenue lors de la création du compte: ".$dbAccess->getAddUserError().'<br />';
else
    echo 'Utilisateur créé avec succès.';

?>

<form action=""
